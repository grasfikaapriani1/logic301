package Logic03;

import java.util.Scanner;

public class Soal10 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number of sequence: ");
        String a = input.nextLine();
        String b = input.nextLine();

        String[] astr = a.split(" ");
        String[] bstr = b.split(" ");
        int[] aint = new int[astr.length];
        int[] bint = new int[bstr.length];
        int pointa = 0;
        int pointb = 0;

        for (int i = 0; i < aint.length; i++) {
            aint[i] = Integer.parseInt(astr[i]);
            bint[i] = Integer.parseInt(bstr[i]);
            if (aint[i] > bint[i]) {
                pointa += 1;
            } else if (aint[i] == bint[i]) {
                pointa += 0;
                pointb += 0;
            } else {
                pointb += 1;
            }
        }
        System.out.println("Output : ");
        System.out.println(pointa + " " + pointb);
    }
}