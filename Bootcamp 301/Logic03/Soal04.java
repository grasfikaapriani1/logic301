package Logic03;

import java.util.Scanner;

public class Soal04 {
    public static int Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukan nilai = \n");
        int d1 = 0;
        int d2 = 0;
        for (int i = 0; i < 3; i++) {
            String matriks[] = input.nextLine().split(" ");
            d1 = d1 + Integer.parseInt(matriks[i]);
            d2 = d2 + Integer.parseInt(matriks[3 - 1 - i]);
        }
        int answer;
        answer = Math.abs(d1 - d2);
        System.out.print(answer);
        System.out.println();

        return 0;
    }
}
