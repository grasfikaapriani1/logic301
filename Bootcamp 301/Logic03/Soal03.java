package Logic03;

import java.util.Scanner;

public class Soal03 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number sequence: ");
        String numberString = input.nextLine();

        String[] numberStringArray = numberString.split(" ");
        int[] numberInt = new int[numberStringArray.length];
        int answer = 0;

        for (int i = 0; i < numberInt.length; i++) {
            numberInt[i] = Integer.parseInt(numberStringArray[i]);
        }

        for (int i = 0; i < numberInt.length; i++) {
            answer += numberInt[i];
        }

        System.out.println("The answer is: " + answer);
    }
}
