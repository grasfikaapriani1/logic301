//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Logic05;

import java.util.Scanner;

public class Soal08 {
    public Soal08() {
    }

    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("please enter the values of n = ");
        int batasan = input.nextInt();
        int fibonaci = 1;
        int bilAwal = 0;
        int bilAkhir = 1;
        int helper = 0;
        int[] array = new int[batasan];
        int[] arrayFibonaci = new int[batasan];
        int[] arrayPrima = new int[batasan];
        int[] arraySum = new int[batasan];

        int i;
        for(i = 0; i < array.length; ++i) {
            System.out.print(fibonaci + " ");
            arrayFibonaci[i] = fibonaci;
            fibonaci = bilAwal + bilAkhir;
            bilAwal = bilAkhir;
            bilAkhir = fibonaci;
        }

        System.out.println();
        i = 1;

        while(true) {
            int number = 0;

            for(int j = 1; j <= i; ++j) {
                if (i % j == 0) {
                    ++number;
                }
            }

            if (number == 2) {
                arrayPrima[helper] = i;
                ++helper;
                System.out.print(i + " ");
                if (helper == array.length) {
                    System.out.println();
                    System.out.println();

                    for(i = 0; i < array.length; ++i) {
                        arraySum[i] = arrayFibonaci[i] + arrayPrima[i];
                        System.out.print(arraySum[i] + " ");
                    }

                    return;
                }
            }

            ++i;
        }
    }
}