//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Logic05;

import java.util.Scanner;

public class Soal03 {
    public Soal03() {
    }

    static int hitungSumKuadrat(int getTampungangka) {  // FUNGSI MEMBUAT HITING HASIL KUADRAT
        int sumKuadrat = 0;
        String numberString = Integer.toString(getTampungangka); // deret
        char[] charArray = numberString.toCharArray(); // di konvert char jadi 1,0,0
        int[] digitArray = new int[charArray.length];

        int i;
        for(i = 0; i < charArray.length; ++i) {
            digitArray[i] = Character.getNumericValue(charArray[i]); //mengembalikan dari str 1 menjadi  int 1
        }

        for(i = 0; i < digitArray.length; ++i) {
            sumKuadrat = (int)((double)sumKuadrat + Math.pow((double)digitArray[i], 2.0));
        } // jumlah hasil kuadrat

        return sumKuadrat;
    }

    public static void Resolve() {
        System.out.print("Masukan banyak deret (n) : ");  //INPUT N
        Scanner input = new Scanner(System.in);
        int inputNumber = input.nextInt(); // DISIMPAN DI INPUT NUMBER
        int deret = 100;  //
        int helper = 0;  // helper membantu stop inputan satu sebanyak n
        int i = 0;

        while(true) {  // while dengan kondisi true terus
            int tampungAngka = deret;   // beisi angka deret 1-100

            do {
                tampungAngka = hitungSumKuadrat(tampungAngka);
            } while(tampungAngka >= 10);        // tampung angka masuk ke hitung 1 kuadrat
                //jika nilai tidak memenuhi maka lgs ke if
            if (tampungAngka == 1) {
                ++helper;
                System.out.println(deret);
            }

            if (helper == inputNumber) {
                return;
            }

            ++deret;
            ++i;
        }
    }
}