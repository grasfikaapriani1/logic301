package Logic05;

import java.util.Scanner;

public class Main {
    public Main() {
    }
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while(answer.toUpperCase().equals("Y"))
        {
            System.out.println("Please enter the question number: ");
            int number = input.nextInt();

            switch (number)
            {
                case 1:
                    Logic05.Soal01.Resolve();
                    break;
                case 2:
                    Soal02.Resolve();
                    break;
                case 3:
                    Soal03.Resolve();
                    break;
                case 4:
                    Soal04.Resolve();
                    break;
                case 5:
                    Soal05.Resolve();
                    break;
                case 6:
                    Soal06.Resolve();
                    break;
                case 7:
                    Soal07.Resolve();
                    break;
                case 8:
                    Soal08.Resolve();
                    break;
                case 9:
                    Soal09.Resolve();
                    break;
                case 10:

                    Soal10.Resolve();
                    break;

                default:
                    System.out.println("No question available");
                    break;
            }

            System.out.println("Again? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }

        System.out.println("Thank you. See you soon");

    }
}