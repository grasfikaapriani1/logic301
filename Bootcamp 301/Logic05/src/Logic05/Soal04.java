//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Logic05;

import java.util.Scanner;

public class Soal04 {
    public Soal04() {
    }

    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Soal 03");
        System.out.println("Menghitung Penggunaan BBM");
        System.out.println("1. Toko");
        System.out.println("2. Tempat 1");
        System.out.println("3. Tempat 2");
        System.out.println("4. tempat 3");
        System.out.println("5. Tempat 4");
        System.out.println("Masukkan Berapa Rute: ");
        int rute = input.nextInt();
        int[] array = new int[rute];
        int jarak = 0;

        int i;
        for(i = 0; i < array.length; ++i) {
            System.out.println(" Masukkan Rute = ");
            int pilihan = input.nextInt();
            array[i] = pilihan;
        }

        for(i = 0; i < array.length - 1; ++i) {
            if (array[i] == 1 && array[i + 1] == 2) {
                jarak += 2000;
            } else if (array[i] == 1 && array[i + 1] == 3) {
                jarak += 2500;
            } else if (array[i] == 1 && array[i + 1] == 4) {
                jarak += 4000;
            } else if (array[i] == 1 && array[i + 1] == 5) {
                jarak += 6500;
            } else if (array[i] == 2 && array[i + 1] == 1) {
                jarak += 500;
            } else if (array[i] == 2 && array[i + 1] == 3) {
                jarak += 500;
            } else if (array[i] == 2 && array[i + 1] == 4) {
                jarak += 2000;
            } else if (array[i] == 2 && array[i + 1] == 5) {
                jarak += 4500;
            } else if (array[i] == 3 && array[i + 1] == 1) {
                jarak += 2500;
            } else if (array[i] == 3 && array[i + 1] == 2) {
                jarak += 1500;
            } else if (array[i] == 3 && array[i + 1] == 4) {
                jarak += 1500;
            } else if (array[i] == 3 && array[i + 1] == 5) {
                jarak += 4000;
            } else if (array[i] == 4 && array[i + 1] == 1) {
                jarak += 4000;
            } else if (array[i] == 4 && array[i + 1] == 2) {
                jarak += 2000;
            } else if (array[i] == 4 && array[i + 1] == 3) {
                jarak += 1500;
            } else if (array[i] == 4 && array[i + 1] == 5) {
                jarak += 2500;
            } else if (array[i] == 5 && array[i + 1] == 1) {
                jarak += 6500;
            } else if (array[i] == 5 && array[i + 1] == 2) {
                jarak += 4500;
            } else if (array[i] == 5 && array[i + 1] == 3) {
                jarak += 4000;
            } else if (array[i] == 5 && array[i + 1] == 4) {
                jarak += 2500;
            }
        }

        System.out.println("BBM = " + jarak / 2500 + " liter");
    }
}