package TugasLogic;

import java.util.Arrays;
import java.util.Scanner;

public class Soal02 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        //Input Text
        System.out.print("Masukkan kalimat : ");
        String stringtext = input.nextLine();

        //Change Text to LowerCase
        stringtext = stringtext.toLowerCase();
        String[] StringArray = stringtext.split(" ");

        //Empty Vokal and Konsonan
        String vokal = "";
        String konsonan = "";

        //Proses membagi konsonan dan vokal
        for (int i = 0; i < StringArray.length; i++) {
            for (int j = 0; j < StringArray[i].length(); j++) {
                if (StringArray[i].charAt(j) == 'a' || StringArray[i].charAt(j) == 'i' || StringArray[i].charAt(j) == 'e' || StringArray[i].charAt(j) == 'o') {
                    vokal = vokal + StringArray[i].charAt(j);
                } else {
                    konsonan = konsonan + StringArray[i].charAt(j);
                }
            }
        }
        //Ubah tipe char vokal dan konsonan kedalam array
        char[] vokal2 = vokal.toCharArray();
        char[] konsonan2 = konsonan.toCharArray();

        //Sorting mengurutkan vokal dan konsonan sesuai abjad
        Arrays.sort(vokal2);
        Arrays.sort(konsonan2);

        //Cetak Output
        System.out.println("Output : ");
        System.out.print("Huruf Vocal : ");
        System.out.println(vokal2);
        System.out.print("Huruf Konsonan : ");
        System.out.println(konsonan2);
    }
}