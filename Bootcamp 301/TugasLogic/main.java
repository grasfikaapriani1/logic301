package TugasLogic;

import TugasLogic.Soal01;
import TugasLogic.Soal03;
import TugasLogic.Soal04;
import TugasLogic.Soal05;
import TugasLogic.Soal06;
import TugasLogic.Soal07;
import TugasLogic.Soal08;
import TugasLogic.Soal09;

import java.util.Scanner;

public class main {
        public static void main(String[] args) {

            Scanner input = new Scanner(System.in);

            String answer = "Y";

            while(answer.toUpperCase().equals("Y"))
            {
                System.out.print("Please enter the question number: ");
                int number = input.nextInt();

                switch (number)
                {
                    case 1:
                        Soal01.Resolve();
                        break;
                    case 2:
                        Soal02.Resolve();
                        break;
                    case 3:
                        Soal03.Resolve();
                        break;
                    case 4:
                        Soal04.Resolve();
                        break;
                    case 5:
                        Soal05.Resolve();
                        break;
                    case 6:
                        Soal06.Resolve();
                        break;
                    case 7:
                        Soal07.Resolve();
                        break;
                    case 8:
                        Soal08.Resolve();
                        break;
                    case 9:
                        Soal09.Resolve();
                        break;
                    default:
                        System.out.println("No question available");
                        break;
                }

                System.out.println("Again? (Y/N)");
                input.nextLine();
                answer = input.nextLine();
            }

            System.out.println("Thank you. See you soon");

        }
    }


