package TugasLogic;

import java.util.Random;
import java.util.Scanner;

public class Soal07 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Jumlah Kartu Atau Gambaran = ");
        int kartu = input.nextInt();

        while(kartu > 0) {
            System.out.println("Masukkan Jumlah Tawaran = ");

            int tawaran;
            for(tawaran = input.nextInt(); tawaran > kartu; tawaran = input.nextInt()) {
                System.out.println("melebihi batas kartu");
                System.out.println("Masukkan Jumlah Tawaran = ");
            }

            int kotakA = 0;
            int kotakB = 0;
            int pilihan = 0;
            int lawan = 0;
            String pesan = " ";
            String menyerah = " ";
            System.out.println();
            Random random = new Random();
            int random1 = random.nextInt(10);
            int random2 = random.nextInt(10);
            System.out.print("Pilihan Kotak  A atau B ?");
            char kotak = input.next().charAt(0);
            if (kotak == 'B' || kotak == 'b') {
                if (kotak == 'B' || kotak == 'b') {
                    kotakB = random2;
                    pilihan = random2;
                    lawan = random1;
                    kotakA = random1;
                }
            } else {
                kotakA = random1;
                pilihan = random1;
                lawan = random2;
                kotakB = random2;
            }

            if (pilihan < lawan) {
                kartu -= tawaran;
                pesan = "You Lose";
            } else if (pilihan > lawan) {
                kartu += tawaran;
                pesan = "You Win";
            } else {
                System.out.println("Draw");
            }

            System.out.println("nilai kotak A = " + kotakA + " Kotak B = " + kotakB + " Kartu = " + kartu);
            System.out.println(pesan);
            if (kartu != 0 && kartu >= 0) {
                System.out.println("Kartu Anda Saat ini : " + kartu);
                System.out.println("Anda Menyerah?");
                input.nextLine();
                System.out.println("Y / N");
                menyerah = input.nextLine();
            } else {
                System.out.println("Kartu Habis / Anda Kalah");
            }

            if (menyerah.equals("y") || menyerah.equals("Y")) {
                break;
            }
        }
    }
}
