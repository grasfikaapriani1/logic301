package TugasLogic;

import java.util.Scanner;

public class Soal08 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan panjangnya deret : ");
        int batasan = input.nextInt();
        int fibonaci = 1; //fibo dimulai dari 1
        int bilAwal = 0;
        int bilAkhir = 1;
        int helper = 0; //membantu menyimpan panjang array dgn bil prima dan fibo

        int[] array = new int[batasan];
        int[] arrayFibonaci = new int[batasan];
        int[] arrayPrima = new int[batasan];
        int[] arraySum = new int[batasan];

        //deret bilangan fibonacci
        int i;
        for(i = 0; i < array.length; i++) {
            System.out.print(fibonaci + " ");
            arrayFibonaci[i] = fibonaci;
            //rumusfibonacci
            fibonaci = bilAwal + bilAkhir;
            bilAwal = bilAkhir;
            bilAkhir = fibonaci;
        }

        System.out.println();
        i = 1;

        //deret bilangan prima
        while(true) { //eksekusi untuk mendapat nilai deret prima dan panjang array yg dijumlahkan
            int number = 0; //

            for(int j = 1; j <= i; j++) {
                if (i % j == 0) { //bilangan prima
                    ++number;
                }
            }

            if (number == 2) { //bil. prima adalah bilangan yg nilainya habis dibagi dengan dirinya sendiri
                arrayPrima[helper] = i;
                ++helper;
                System.out.print(i + " ");
                if (helper == array.length) {
                    System.out.println();
                    System.out.println();

                    //penjumlahan deret fibo dan deret prima
                    for(i = 0; i < array.length; i++) {
                        arraySum[i] = arrayFibonaci[i] + arrayPrima[i];
                        System.out.print(arraySum[i] + " ");
                    }

                    return;
                }
            }

            i++;
        }
    }
}
