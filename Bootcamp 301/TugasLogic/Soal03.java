package TugasLogic;

import java.util.Scanner;

public class Soal03 {
    static int hitungSumKuadrat(int getTampungangka) { // fungsi menghitung hasil kuadrat input
        int sumKuadrat = 0;
        String numberString = Integer.toString(getTampungangka);
        char[] charArray = numberString.toCharArray();
        int[] digitArray = new int[charArray.length];

        int i;
        for (i = 0; i < charArray.length; i++) {
            digitArray[i] = Character.getNumericValue(charArray[i]); //mengembalikan nilai awal (ex: dari string 1 jadi int 1)
        }

        for (i = 0; i < digitArray.length; i++) {
            sumKuadrat = (int) (sumKuadrat + Math.pow(digitArray[i], 2));
        }

        return sumKuadrat;
    }

    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan banyak deret (n) : ");
        int inputNumber = input.nextInt();
        int deret = 100; //awal deret dimulai 100
        int helper = 0; //bantu menyimpan si angka 1 sebanyak n
        int i = 0;

        while (true) {  //akan di eksekusi terus karena bernilai true
            int tampungAngka = deret;

            do {
                tampungAngka = hitungSumKuadrat(tampungAngka);
            } while (tampungAngka >= 10); //

            if (tampungAngka == 1) {
                ++helper;
                System.out.println(+deret+  " si angka 1");
            }

            if (helper == inputNumber) {
                return;
            }

            ++deret;
            i++;
        }
    }
}
