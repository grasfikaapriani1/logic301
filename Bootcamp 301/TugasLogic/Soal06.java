package TugasLogic;

import java.util.Scanner;

public class Soal06 {
    public static void Resolve() {
        String pin = "123456";
        long saldo = 0L;

        char ulangTransaksi;
        do {
            Scanner input = new Scanner(System.in);
            System.out.print("Masukan PIN = ");
            String inputPin = input.nextLine();
            if (pin.equals(inputPin)) {
                System.out.print("Masukan Nominal Setor tunai : ");
                long inputSetor = (long)input.nextInt();
                saldo += inputSetor;
                System.out.println("Setor Tunai Berhasil");
                System.out.println("Saldo anda saat ini : " + saldo);
                System.out.println();
                System.out.println("1.Antar Rekening \n2.Antar Bank");
                System.out.print("Pilih Jenis Transfer : ");
                int menuTransfer = input.nextInt();
                if (menuTransfer == 1) {
                    System.out.println("Transfer Sesama Bank");
                    System.out.print("Masukan Nomor Rekening Tujuan : ");
                    long noRek1 = input.nextLong();
                    System.out.print("Masukan Nominal Transfer : ");
                    long nominalTransfer = input.nextLong();
                    if (nominalTransfer >= saldo) {
                        System.out.println("Saldo Tidak Cukup");
                    } else if (nominalTransfer <= saldo) {
                        System.out.println("Tranfer ke Nomor Rekening : " + noRek1);
                        System.out.println("Jumlah Nominal transfer : " + nominalTransfer);
                        System.out.println("Transfer berhasil");
                        saldo -= nominalTransfer;
                        System.out.println("Saldo Anda saat ini : " + saldo);
                    }
                } else if (menuTransfer == 2) {
                    System.out.print("Masukan kode Bank : ");
                    int kodeBank = input.nextInt();
                    System.out.print("Masukan Nomor Rekening Tujuan : ");
                    long noRek1 = input.nextLong();
                    System.out.print("Masukan Nominal Transfer : ");
                    long nominalTransfer = input.nextLong();
                    if (nominalTransfer + 7500L >= saldo) {
                        System.out.println("Saldo Tidak Cukup");
                    } else if (nominalTransfer <= saldo + 7500L) {
                        System.out.println("Tranfer ke Nomor Rekening : " + noRek1);
                        System.out.println("Jumlah Nominal transfer : " + nominalTransfer);
                        System.out.println("Transfer berhasil");
                        saldo -= nominalTransfer + 7500L;
                        System.out.println("Saldo Anda saat ini : " + saldo);
                    }
                }
            } else {
                System.out.println("PIN yang Anda masukan salah");
            }

            System.out.println("\nIngin transaksi lagi! (y/n)");
            ulangTransaksi = input.next().charAt(0);
        } while(ulangTransaksi == 'y' && ulangTransaksi != 'n');

    }
}
