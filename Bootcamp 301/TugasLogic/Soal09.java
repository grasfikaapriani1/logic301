package TugasLogic;

import java.util.Scanner;

public class Soal09 {
    public static void Resolve() {
        System.out.println("Soal 09");
        System.out.println("Keterangan Pola : ");
        System.out.println("N = naik, T = turun");
        System.out.println("\n");
        System.out.print("Inputkan pola : ");
        Scanner input = new Scanner(System.in);
        String pola = input.nextLine();
        char[] arrayPola = pola.toCharArray();
        int mdpl = 0;
        int gunung = 0;
        int lembah = 0;

        int i;
        for(i = 0; i < arrayPola.length; i++) {
            if (arrayPola[i] == 'N') {
                ++mdpl;
                if (mdpl == 0) {
                    ++lembah;
                }
            } else if (arrayPola[i] == 'T') {
                --mdpl;
                if (mdpl == 0) {
                    ++gunung;
                }
            }
        }

        for(i = 0; i < arrayPola.length; i++) {
            System.out.print(arrayPola[i] + " ");
        }

        System.out.println("\n");
        System.out.println("Jumlah Gunung " + gunung);
        System.out.println("Jumlah Lembah " + lembah);
    }
}
