package TugasLogic;

import java.util.Scanner;
public class Soal05 {
    public static void Resolve() {

        //menggunakan float karena ada porsi yg bernilai desimal
        float totalLaki = 0.0F;
        float totalPerempuan = 0.0F;
        float totalRemaja = 0.0F;
        float totalAnak = 0.0F;
        float totalBalita = 0.0F;
        System.out.println("1. Laki-laki dewasa");
        System.out.println("2. Perempuan dewasa");
        System.out.println("3. Remaja");
        System.out.println("4. Anak-anak");
        System.out.println("5. Balita");

        char ulangMenu;
        do {
            System.out.println("Pilih rentang usia : ");
            Scanner input = new Scanner(System.in);
            int inputCase = input.nextInt();
            switch (inputCase) {
                case 1:
                    System.out.print("Masukan jumlah orang laki-laki dewasa : ");
                    int lakiDewasa = input.nextInt();
                    totalLaki += (float)lakiDewasa;
                    break;
                case 2:
                    System.out.print("Masukan jumlah orang perempuan dewasa : ");
                    int perempuanDewasa = input.nextInt();
                    totalPerempuan += (float)perempuanDewasa;
                    break;
                case 3:
                    System.out.print("Masukan jumlah orang remaja : ");
                    int remaja = input.nextInt();
                    totalRemaja += (float)remaja;
                    break;
                case 4:
                    System.out.print("Masukan jumlah orang anak-anak : ");
                    int anak = input.nextInt();
                    totalAnak += (float)anak;
                    break;
                case 5:
                    System.out.print("Masukan jumlah orang balita : ");
                    int balita = input.nextInt();
                    totalBalita += (float)balita;
                    break;
                default:
                    System.out.print("Inputan salah. Pilih angka 1 sampai 5!");
            }

            do {
                System.out.println("Masih ingin input (y/n)? ");
                ulangMenu = input.next().charAt(0);
                if (ulangMenu != 'n' && ulangMenu != 'y') {
                    System.out.println("Maaf inputan tidak sesuai");
                }
            } while(ulangMenu != 'n' && ulangMenu != 'y');
        } while(ulangMenu != 'n' && ulangMenu == 'y'); //jika menginputkan n maka akan berhenti karena false

        //kondisi constraint
        float totalOrang = totalLaki + totalPerempuan + totalRemaja + totalAnak + totalBalita;
        float porsiPerempuan;
        if (totalOrang > 5.0F && totalOrang % 2.0F != 0.0F) {
            porsiPerempuan = totalPerempuan * 2.0F;
        } else {
            porsiPerempuan = totalPerempuan * 1.0F;
        }

        float porsiLaki = totalLaki * 2.0F;
        float porsiAnak = totalAnak * 1.0F / 2.0F;
        float porsiBalita = totalBalita * 1.0F;
        float porsiRemaja = totalRemaja * 1.0F;
        float totalPorsi = porsiLaki + porsiPerempuan + porsiAnak + porsiBalita + porsiRemaja;
        System.out.println("\nJumlah laki-laki dewasa: " + (int)totalLaki);
        System.out.println("Jumlah perempuan dewasa: " + (int)totalPerempuan);
        System.out.println("Jumlah remaja: " + (int)totalRemaja);
        System.out.println("Jumlah anak-anak: " + (int)totalAnak);
        System.out.println("Jumlah balita: " + (int)totalBalita);
        System.out.println("Total Orang: " + (int)totalOrang);
        System.out.println("Total Porsi: " + totalPorsi);
    }
}
