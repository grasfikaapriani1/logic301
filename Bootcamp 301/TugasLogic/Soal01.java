package TugasLogic;

import java.util.Scanner;

public class Soal01 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("ANGKA GANJIL GENAP");
        System.out.println("Soal 01");
        System.out.println("Please enter the value of n: ");
        int n = input.nextInt();
        int[][] arrayInput = new int[2][n];
        int bilangangenap = 2;
        int bilanganganjil = 1;

        int i;
        int j;
        for(i = 0; i < arrayInput.length; ++i) {
            for(j = 0; j < arrayInput[0].length; ++j) {
                if (i == 0 && bilanganganjil % 2 == 1 && bilanganganjil <= n) {
                    arrayInput[i][j] = bilanganganjil;
                    bilanganganjil += 2;
                } else if (i == 1 && bilangangenap % 2 == 0 && bilangangenap <= n) {
                    arrayInput[i][j] = bilangangenap;
                    bilangangenap += 2;
                }
            }
        }

        for(i = 0; i < arrayInput.length; ++i) {
            for(j = 0; j < arrayInput[0].length; ++j) {
                if (arrayInput[i][j] != 0) {
                    System.out.print(arrayInput[i][j] + " ");
                }
            }

            System.out.println("");
        }

    }
}
