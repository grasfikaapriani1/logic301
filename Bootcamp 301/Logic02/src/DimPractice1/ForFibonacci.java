package DimPractice1;

import java.util.Scanner;

public class ForFibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		// Nomor 11
		System.out.println("Nomor 11");
		// Prompt input
		System.out.print("Input value of n:");
		int n = input.nextInt();

        // Prerequisite
        int[] arrayInteger = new int[n];
        int a=1;
	    int b=1;
	    int c=1;

        // Enter the value to array variable
        for (int i = 0; i < n; i++) {
            arrayInteger[i] = a;
            c=a+b; //rumus fibonacci
            a=b;
            b=c;
        }

        // Print array
        for (int i = 0; i < n; i++) {
        	System.out.print(arrayInteger[i] + " ");
        }

        System.out.println();
        input.close();
	}

}
