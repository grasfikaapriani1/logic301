package DimPractice1;
import java.util.Scanner;

public class ForOdd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		// Nomor 01
		System.out.println("Nomor 01");
		// Prompt input
		System.out.print("Input value of n:");
		int n = input.nextInt();

        // Prerequisite
        int[] arrayInteger = new int[n];
        int helper = 1;

        // Enter the value to array variable
        for (int i = 0; i < n; i++) {
            arrayInteger[i] = helper;
            helper += 2;
        }

        // Print array
        for (int i = 0; i < n; i++) {
            System.out.print(arrayInteger[i] + " ");
        }

        System.out.println();
        input.close();
	}
}