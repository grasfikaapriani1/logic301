package DimPractice1;

import java.util.Scanner;

public class ForNum10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		// Nomor 10
		System.out.println("Nomor 10");
		// Prompt input
		System.out.print("Input value of n:");
		int n = input.nextInt();

        // Prerequisite
        int[] arrayInteger = new int[n];
        int helper = 3;

        // Enter the value to array variable
        for (int i = 0; i < n; i++) {
            arrayInteger[i] = helper;
            helper *= 3;
        }

        // Print array
        for (int i = 0; i < n; i++) {
        	if(i==2) {
        		System.out.print(arrayInteger[i] + " XXX ");
        	}
        	else {
        		System.out.print(arrayInteger[i] + " ");
        	}
        }

        System.out.println();
        input.close();
	}

}
