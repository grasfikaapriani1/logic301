package DimPractice2;

import java.util.Scanner;

public class Soal01 {

	public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter the value of n: ");
        int n = input.nextInt();
        System.out.println("Please enter the value of n2: ");
        int n2 = input.nextInt();

        // Prerequisite
        int[][] array2dInteger = new int[2][n];

        for (int i = 0; i < array2dInteger.length; i++) {
            for (int j = 0; j < array2dInteger[0].length; j++) {
                if ( i == 0 )
                {
                    array2dInteger[i][j] = j;
                }
                else
                {
                    array2dInteger[i][j] = (int) Math.pow(n2, j);
                }
            }
        }

        // Cetak Array
        for (int i = 0; i < array2dInteger.length; i++) {
            for (int j = 0; j < array2dInteger[0].length; j++) {
                System.out.print(array2dInteger[i][j] + " ");
            }
            System.out.println();
        }
        input.close();
        
    }
}
