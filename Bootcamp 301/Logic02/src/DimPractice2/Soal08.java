package DimPractice2;

import java.util.Scanner;

public class Soal08 {

	public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter the value of n: ");
        int n = input.nextInt();

        // Prerequisite
        int[][] array2dInteger = new int[3][n];

        for (int i = 0; i < array2dInteger.length; i++) {
            for (int j = 0; j < array2dInteger[0].length; j++) {
                if ( i == 0 )
                {
                    array2dInteger[i][j] = j;
                }
                else if(i==1){
		        	array2dInteger[i][j] = 2*j;
		        }
		        else{
		        	array2dInteger[i][j]= 3*j;
		        }
            }
        }

        // Cetak Array
        for (int i = 0; i < array2dInteger.length; i++) {
            for (int j = 0; j < array2dInteger[0].length; j++) {
                System.out.print(array2dInteger[i][j] + " ");
            }
            System.out.println();
        }
        input.close();
        
    }
}
