package com.company.Logic01.src;
import java.util.Scanner;
public class no5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Nomor 01
        // Prompt input
        System.out.println("Please enter the value of n: ");
        int n = input.nextInt();

        // Prerequisite
        int[] arrayInteger = new int[n];
        int helper = 1;

        // Enter the value to array variable
        for (int i = 0; i < n; i++) {
            arrayInteger[i] = helper;
            helper += 4;
        }

        // Print array
        for (int i = 0; i < n-2; i++) {
            if(i%2==0) {
                System.out.print(arrayInteger[i] + "  ");
            }
            else {
                System.out.print(arrayInteger[i] + " * ");
            }
        }

        System.out.println();

    }
}
