package com.company.Logic01.src;
import java.util.Scanner;
public class no11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Nomor 01
        // Prompt input
        System.out.println("Please enter the value of n: ");
        int n = input.nextInt();

        // Prerequisite
        int[] arrayInteger = new int[n];
        int a,b,c;
        a = 0;
        b = 1;
        c = 1;

        // Enter the value to array variable
        for (int i = 0; i < n; i++) {
            arrayInteger[i] = b;
            c = a + b;
            a = b;
            b = c;
        }

        // Print array
        for (int i = 0; i < n; i++) {
            System.out.print(arrayInteger[i] + " ");
        }

        System.out.println();

    }

}
