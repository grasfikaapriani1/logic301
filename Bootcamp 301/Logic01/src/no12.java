package com.company.Logic01.src;
import java.util.Scanner;
public class no12 {
    public static void main(String[] args) {
        // inisialisasi Fungsi
        Scanner input = new Scanner(System.in);
        // deklarasi variabel
        int bilangan, n;

        System.out.print("Input n: ");
        n = input.nextInt();

        for (int i=0; i<n; i++)
        {
            bilangan=0;
            for (int j=0; j<20; j++)
            {
                if (i%j==0)
                {
                    bilangan = bilangan+1;
                }
            }
            if (bilangan==2)
            {
                System.out.print(i+" ");
            }
        }
    }
}