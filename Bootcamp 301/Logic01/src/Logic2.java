package Logic01.src;

public class Logic2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//NO.1
		System.out.print("[NO.1]");
		System.out.println();
			int a,b;
			int c=9;
				for(a=1; a<=c; a++) {
		            for(b=1; b<=c; b++) {
		                if (a==b){
		                    System.out.print(a+b-1); //untuk hasil ganjil
		                } else {
		                    System.out.print(" ");
		                }
		            }
		            System.out.println();
		        }
				
				System.out.println();
		
		//NO.2
			System.out.print("[NO.2]");
			System.out.println();
			int d,e;
			int f=9;
			 for(d=0; d<f; d++) {
			     for(e=0; e<f; e++) {
			        if ((e+d)==(f-1)){ // bilangan dari kanan atas ke bawah - diagonal
			        		System.out.print(e*2);
			        } else {
			                System.out.print(" ");
			        }
			      }
			         System.out.println();
			 }
			 
			 System.out.println();
				
				
		//NO.3
			 System.out.print("[NO.3]");
			 System.out.println();
			 int g,h;
			 int i=9;
			 	for(g=1; g<=i; g++) {
			 		for(h=1; h<=i; h++) {
					    if (g==h){
					       System.out.print(g+h-1); //diagonal kiri
					    } else if((g+h)==(i+1)){
					       System.out.print((h-1)*2); //diagonal kanan
					    }
					      else{
					       System.out.print(" ");
					    }
					 }
					  	System.out.println();
			 	}
			 	
			 	System.out.println();
				
		//NO.4
			 	System.out.print("[NO.4]");
			 	System.out.println();
			 	int j,k;
				 int l=9;
				 	for(j=1; j<=l; j++) {
				 		for(k=1; k<=l; k++) {
						    if (j==k){
						       System.out.print(j+k-1); //diagonal kanan
						       System.out.print(" ");
						    } 
						    else if((j+k)==(l+1)){
						       System.out.print((k-1)*2); //diagonal kiri
						       System.out.print(" ");
						    }
						    else if(k==5){
						    	System.out.print(j*2-1); //diagonal vertikal
						    	System.out.print(" ");
						    }
						    else if(j==5){
						    	System.out.print((k-1)*2); //diagonal horizontal
						    }
						    else{
						       System.out.print(" ");
						    }
						 }
						  	System.out.println();
				 	}
				 	
				 	System.out.println();
				 	
		//NO.5
				 	System.out.print("[NO.5]");
				 	System.out.println();
				 	for(int m=1; m<=9; m++) {
				 		for(int n=1; n<=9; n++) {
				 			if(m>=n) {
				 				System.out.print(n*2-1);
				 			}
				 			else {
				 				System.out.print(" ");
				 			}
				 		}
			 			System.out.println();
				 	}
				 	
				 	System.out.println();
				 	
		//NO.6
				 	System.out.print("[NO.6]");
				 	System.out.println();
				 	/*for(int o=9; o>=1; o--) {
				 		for(int p=9; p>=1; p--) {
				 			if((o+p)<=9+1) {
				 				System.out.print((o-1)*2+" ");
				 			}
				 			else {
				 				System.out.print(" ");
				 			}
				 		}
			 			System.out.println();
				 	}*/
				 	for(int o=1; o<=9; o++) {
				 		for(int p=o; p<=9; p++) {
				 			System.out.print(" ");
				 		}
				 		for(int p=1; p<=o; p++) {
				 			if((o+p)<=9-1) {
				 				System.out.print((o-1)*2+" ");
				 			}
				 			else {
				 				System.out.print(" ");
				 			}
				 		}
				 		System.out.println();
				 	}
				 	
				 	System.out.println();
				 	
		//NO.7
				 	System.out.print("[NO.7]");
				 	System.out.println();
				 	for (int q=1; q<=9; q++) {
						for(int r=1; r<=9; r++) {
							if (q==r)
								System.out.print(q+r-1);
							else if ((q+r)==(9+1))
								System.out.print((r-1)*2);
							else if (q<r&&(q+r)<10)
								System.out.print(" A");
							else if (q>r&&(q+r)>10)
								System.out.print(" B");
							else
								System.out.print("  ");
						}System.out.println();
				 	}	
				 	System.out.println();
				 	
		//NO.8
				 	System.out.print("[NO.8]");
				 	System.out.println();
				 	for (int s=1; s<=9; s++) {
						for(int t=1; t<=9; t++) {
							if (s==t)
								System.out.print(s+t-1);
							else if ((s+t)==(9+1))
								System.out.print((t-1)*2);
							else if (s>t&&(s+t)<10)
								System.out.print(" A");
							else if (s<t&&(s+t)>10)
								System.out.print(" B");
							else
								System.out.print("  ");
						}System.out.println();
				 	}	
				 	System.out.println();
				 	
				 	
		//NO.9
				 	System.out.print("[NO.9]");
				 	System.out.println();	 	

				 	int n = 9;
			        int kelipatan = 1;
			        for (int u = 0; u < n; u++) {
			            for (int v = 0; v < n; v++) {
			                System.out.print(kelipatan + " ");
			                if (v < ((n) / 2)) { //kondisi di dalam for nested
			                    kelipatan += 2;
			                } else {
			                    kelipatan -= 2;
			                }

			            }
			            kelipatan = 1;
			            System.out.println();
			        }
			        System.out.println();
			        
			      //NO.10
				 	System.out.print("[NO.10]");
				 	System.out.println();
				 	int z = 9;
				    int kelipatan1 = 0;
				        for (int x = 0; x < z; x++) {
				            for (int y = 0; y < z; y++) {
				                System.out.print(kelipatan1 + " ");
				            }
				            if (x < ((n) / 2)) { //kondisi di dalam for pertama
				                kelipatan1 += 2;
				            } else {
				                kelipatan1 -= 2;
				            }
				            System.out.println();
				        }
				        
				        System.out.println();   
				        
				     //POST TEST
				        System.out.print("[POST TEST]");
					 	System.out.println();
					 	/*int ii;
						 	for(ii=1; ii<=9; ii++) {
						 		//for(kk=1; kk<=9; kk++) {
								    if (ii==5){
								    	int limit=8, past, current, fibonacci;
								        past = 1;
								        current = 1;
								        fibonacci = 1;
								        for (int fi =1; fi <= limit; fi++){
								            System.out.print( current);
								            fibonacci = past + current;
								            past = current;
								            current = fibonacci;
								            System.out.print(" ");
								            System.out.println(); 
								        }
								      
								       
								    } 
								   
								 }*/
					 	 int aa=0;
					      int bb=1;
					      int cc=1;
					      int nn=9;
					      for(int ii=1;ii<=n;ii++){
					          for(int jj=1;jj<=nn;jj++){
					              if(jj==5){
					                  System.out.print(" "+cc);
					                  cc=aa+bb;
					                  aa = bb;
					                  bb = cc;
					              }
					              else if(ii+jj==10&&jj<5){
					                  System.out.print(" "+bb);
					              }
					              else if(ii==jj&&jj>5){
					                  System.out.print(" "+aa);
					              }
					              else{
					                  System.out.print(" *");
					              }
					          }
					          System.out.println();
					      }
					      
					      System.out.println();
					      
				//PR	  System.out.print("[PR]");
					 /*     System.out.println();
					      int ax=0;
					      int bx=1;
					      int cx=1;
					      int nx=9;
					      for(int ix=1;ix<=nx;ix++){
					          for(int jx=1;jx<=nx;jx++){
					              if(jx==5){
					                  System.out.print(" "+cx);
					                  cx = ax+bx;
					                  ax = bx;
					                  bx = cx;
					              }
					              else if(ix+jx==nx+1&&jx<5&&jx>1){
					                  System.out.print(" "+(jx-1));
					              }
					              else if(ix==jx&&jx>5&&jx<nx){
					                  System.out.print(" "+((i-nx)*-1));
					              }
					              else if((i==nx&&jx==1)){
					            	  System.out.print(" 1");
					              }
					              else if(ix==nx&&jx==nx) {
					            	  System.out.print(" 1");
					              }
					              else{
					                  System.out.print("  ");
					              }
					          }
					          System.out.println();
					      }		*/ 		 
}
}

