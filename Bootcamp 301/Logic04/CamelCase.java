package String;
import java.util.Scanner;
public class CamelCase {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Input the words : ");
        String Word = input.next();
        int jumlah = 0;
        for (int i = 0; i < Word.length(); i++){
            if (Character.isUpperCase(Word.charAt(i))){
                jumlah++;
            }

        }
        System.out.print(jumlah + 1);
        System.out.println();
    }

    public static class Main {
        public static void main(String[] args) {

            Scanner input = new Scanner(System.in);
            String answer = "Y";
            while(answer.toUpperCase().equals("Y"))
            {
                System.out.println("Please enter the question number: ");
                int number = input.nextInt();
                switch (number)
                {
                    case 1:
                        Resolve();
                        break;
                    case 4:
                        MarsExploration.Resolve();
                        break;
                    case 11:
                        Palindrome.Resolve();
                        break;
                    default:
                        System.out.println("No question available");
                        break;
                }
                System.out.println("Again? (Y/N)");
                input.nextLine();
                answer = input.nextLine();
            }
            System.out.println("Thank you. See you soon");
        }
    }
}
