package Logic04;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Gemstones {
    public static void Resolve()  {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner input = new Scanner(System.in);
        System.out.print("Input berapa baris yang akan dicek: ");
        int n = input.nextInt(); //ganti var
        input.nextLine();

        Set<Character> gemstones = stringToSet(input.nextLine()); //set gemstone

        for (int i = 1; i < n; i++) {
            gemstones.retainAll(stringToSet(input.nextLine())); //Perform intersection
        }
        System.out.print(gemstones.size());
    }


    public static Set<Character> stringToSet(String s) //Converts String to Character set
    {
        Set<Character> set = new HashSet<Character>(26);
        for (char c : s.toCharArray())
            set.add(Character.valueOf(c));
        return set;
    }
}

