package String;
import java.util.Scanner;

public class Palindrome {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan kata : ");

        String str = input.nextLine(), reverseStr = "";

        int strLength = str.length();

        for (int i = (strLength - 1); i >=0; i--) {
            reverseStr = reverseStr + str.charAt(i);
        }

        if (str.toLowerCase().equals(reverseStr.toLowerCase())) {
            System.out.println("Yes");
        }
        else {
            System.out.println("No");
        }
    }
}