package com.company.String;
import java.util.Scanner;
public class pangram {
    public static void main(String[] args) {

        //Scanner is a class which read input from keyboard
        Scanner input = new Scanner(System.in);

        System.out.println("Enter Your Words:");

        //to read string end of line
        String text = input.nextLine();

        //toLowerCase()->method which converts all characters to lower case
        text = text.toLowerCase();

        //empty string
        String helper="";

        // checking characters (a-z or A-Z)
        for(char i='a';i<='z';i++){

            //indexOf(char i)--> This method returns '-1' substring not found, if the position of substrings 'i' in 'text'
            if(text.indexOf(i)!=-1){

                helper=helper+i;// empty string+character
            }
        }
        // helper.length()-->this method returns number or character of a string
        System.out.println("Output : ");
        if(helper.length()==26){
            System.out.println("Pangram");
        }
        else{
            System.out.println("Not Pangram");
        }
    }
}

