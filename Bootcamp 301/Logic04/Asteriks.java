package com.company.String;
import java.util.Scanner;
public class Asteriks {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan kalimat : ");
        String line1 = input.nextLine();
        String line2 = input.nextLine();
        String line3 = input.nextLine();

        System.out.println();

        char[] charArrayline1 = line1.toCharArray();
        char[] charArrayline2 = line2.toCharArray();
        char[] charArrayline3 = line3.toCharArray();

        System.out.println("Output : ");

        for (int i = 0; i < charArrayline1.length; i++) {
            if ((i > 0) && (i < charArrayline1.length - 1)) {
                System.out.print("*");
            } else {
                System.out.print(charArrayline1[i]);
            }
        }
        System.out.print(" ");

        for (int i = 0; i < charArrayline2.length; i++) {
            if ((i > 0) && (i < charArrayline2.length - 1)) {
                System.out.print("*");
            } else {
                System.out.print(charArrayline2[i]);
            }
        }
        System.out.print(" ");
        for (int i = 0; i < charArrayline3.length; i++) {
            if ((i > 0) && (i < charArrayline3.length - 1)) {
                System.out.print("*");
            } else {
                System.out.print(charArrayline3[i]);
            }
        }
        System.out.print(" ");
    }
}
