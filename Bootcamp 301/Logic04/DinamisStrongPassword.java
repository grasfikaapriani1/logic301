package Logic04;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class DinamisStrongPassword {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input password: ");
        String password = input.next();
        String passwordStr = "";

        int passwordLength = password.length();

        boolean hasLower = false, hasUpper = false,
                hasDigit = false, specialChar = false;
        Set<Character> set = new HashSet<Character>(
                Arrays.asList('!', '@', '#', '$', '%', '^', '&',
                        '*', '(', ')', '-', '+'));
        for (char i : password.toCharArray())
        {
            if (Character.isLowerCase(i))
                hasLower = true;
            if (Character.isUpperCase(i))
                hasUpper = true;
            if (Character.isDigit(i))
                hasDigit = true;
            if (set.contains(i))
                specialChar = true;
        }

        // Strength of password
        System.out.print("Strength of password: ");
        if(passwordLength>=6) {
            if (hasDigit && hasLower && hasUpper && specialChar) {
                System.out.print(" Strong");
            } else if (hasDigit && hasLower && hasUpper) {
                System.out.println(" 1");
            } else if (hasLower && hasUpper && specialChar) {
                System.out.println(" 1");
            } else if (hasDigit && hasLower) {
                System.out.println(" 3");
            } else if (hasDigit && hasUpper) {
                System.out.println(" 3");
            } else if (hasDigit && specialChar) {
                System.out.println(" 3");
            } else if (hasLower && hasUpper) {
                System.out.println(" 3");
            } else if (hasLower && specialChar) {
                System.out.println(" 3");
            } else if (hasUpper && specialChar) {
                System.out.println(" 3");
            }
        } else if (passwordLength<6) {
            if (hasDigit && hasLower && hasUpper && specialChar) {
                System.out.print(" 1");
            } else if (hasDigit && hasLower && hasUpper) {
                System.out.println(" 3");
            } else if (hasLower && hasUpper && specialChar) {
                System.out.println(" 3");
            } else if (hasDigit && hasLower) {
                System.out.println(" 4");
            } else if (hasDigit && hasUpper) {
                System.out.println(" 4");
            } else if (hasDigit && specialChar) {
                System.out.println(" 4");
            } else if (hasLower && hasUpper) {
                System.out.println(" 4");
            } else if (hasLower && specialChar) {
                System.out.println(" 4");
            } else if (hasUpper && specialChar) {
                System.out.println(" 4");
            }
        }
    }
}
