package String;
import java.util.Scanner;
public class SeparateTheNumbers {
    public static void Resolva() {
        Scanner input = new Scanner(System.in);
        System.out.println("Input the number sequence: ");
        int s = input.nextInt();
        long a = check(String.valueOf(s));
        if (a != -1) {
            System.out.println("YES" + a);
        } else {
            System.out.println("NO");
        }
    }

    public static long check(String s) {
        for (int j = 1; j < s.length() + 1 && j < 18; j++) {
            long a = Long.parseLong(s.substring(0, j));
            long init = a;
            String temp = "" + a;
            int count = 1;
            while (temp.length() < s.length()) {
                a++;
                count++;
                temp += a;
            }
            if (temp.equals(s) && count >= 2) {
                return init;
            }
        }
        return -1;
    }
}

