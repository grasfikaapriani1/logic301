package LogicPractice;
import java.util.Scanner;
public class Soal5 {
        public static void Resolve() {
            System.out.print("KONVERSI WAKTU");
            System.out.print("\nPilihan Konversi : \n");
            System.out.print("1. 24H ke 12H \n");
            System.out.print("2. 12H ke 24H \n");
            char ulangPilih;
            do {
                Scanner input = new Scanner(System.in);
                System.out.print("Pilih konversi waktu : ");
                int inputPilih = input.nextInt();
                switch(inputPilih) {
                    case 1:
                        System.out.print("Masukan Jam : ");
                        int jam24 = input.nextInt();
                        System.out.print("Masukan menit : ");
                        int menit24 = input.nextInt();
                        if(jam24 < 12 && menit24 <=59 ) {
                            System.out.print(("Pukul ") + jam24 + (":")+ menit24);
                            System.out.print("\nKonversi waktu " + jam24 +":" + menit24 + " AM");
                        }
                        else if(jam24 > 12 && jam24 <=23 && menit24 <=59) {
                            System.out.print(("Pukul ") + jam24 + (":")+ menit24);
                            System.out.print("\nKonversi waktu " + (jam24 - 12) +":" + menit24 + " PM");
                        }
                        else if((jam24 > 24) || (menit24 > 59)){
                            System.out.print("\nFormat tidak valid");
                        }
                        break;
                    case 2:
                        String StringAM = "AM";
                        String StringPM = "PM";
                        System.out.print("Masukan Jam : ");
                        int jam12 = input.nextInt();
                        System.out.print("Masukan menit : ");
                        int menit12 = input.nextInt();
                        System.out.print("AM/PM : ");
                        String AMPM = input.next();
                        if(jam12 < 12 && menit12 <=59 && AMPM.toLowerCase().equals(StringAM.toLowerCase())) {
                            System.out.print(("Pukul ") +jam12 + (":")+ menit12 +(" ")+AMPM);
                            System.out.print("\nKonversi waktu " + jam12 +":" + menit12);
                        }
                        else if(jam12 == 12 && menit12 <=59 && AMPM.toLowerCase().equals(StringAM.toLowerCase())) {
                            System.out.print(("Pukul ") +jam12 + (":")+ menit12 +(" ")+AMPM);
                            System.out.print("\nKonversi waktu " + "00" +":" + menit12);
                        }
                        else if(jam12 < 12 && menit12 <=59 && AMPM.toLowerCase().equals(StringPM.toLowerCase())) {
                            System.out.print(("Pukul ") +jam12 + (":")+ menit12 +(" ")+AMPM);
                            System.out.print("\nKonversi waktu " + (jam12 + 12) +":" + menit12);
                        }
                        else if ((jam12 > 12) || (menit12 > 59)) {
                            System.out.print("\nFormat tidak valid");
                        }
                        break;
                    default:
                        System.out.print("Data yg dimasukan salah. Pilih angka 1 atau 2!");
                }

                do {
                    System.out.print("\nMasih ingin input (y/n)? ");
                    ulangPilih = input.next().charAt(0);
                    if (ulangPilih != 'n' && ulangPilih!= 'y') {
                        System.out.println("Maaf inputan tidak sesuai");
                    }
                } while(ulangPilih != 'n' && ulangPilih != 'y');
            } while(ulangPilih != 'n' && ulangPilih == 'y');
        }
    }