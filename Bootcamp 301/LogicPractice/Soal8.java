package LogicPractice;
import java.util.Scanner;

public class Soal8 {
    public static void Resolve() {

            Scanner input = new Scanner(System.in);

                System.out.print("Masukkan jumlah uang : ");
                int uang = input.nextInt();

                System.out.print("Masukkan banyaknya jenis kacamata : ");
                int jenisKacamata = input.nextInt();

                int[] Kacamata = new int[jenisKacamata];
                for (int i = 0; i < jenisKacamata; i++) {
                    System.out.print("Masukkan harga kacamata ke-" + (i + 1) + ": ");
                    Kacamata[i] = input.nextInt();
                }

                System.out.print("Masukkan banyaknya jenis baju : ");
                int jenisBaju = input.nextInt();

                int[] Baju = new int[jenisBaju];
                for (int i = 0; i < jenisBaju; i++) {
                    System.out.print("Masukkan harga baju ke-" + (i + 1) + ": ");
                    Baju[i] = input.nextInt();
                }
                int helper=0;
                int[] arraySum = new int[(Kacamata.length + Baju.length)];
                for (int i = 0; i < Kacamata.length; i++) {
                    for (int j = 0; j < Baju.length; j++) {
                        if (Kacamata[i] + Baju[j] <= uang) {
                            arraySum[helper] = Kacamata[i] + Baju[j];
                            helper++;
                        }
                    }
                }
                //Menacari nilai maksimum didalam array
                int max = arraySum[0];
                for(int i = 0; i < arraySum.length; i++){
                    if(arraySum[i] > max){
                        max = arraySum[i];
                    }
                }
                System.out.print("Output : ");
                System.out.println(max);
            }
        }


