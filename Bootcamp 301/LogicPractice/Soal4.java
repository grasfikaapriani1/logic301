package LogicPractice;

import java.util.Scanner;
public class Soal4 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Jumlah Uang Rp.");
        int jumlahUang = input.nextInt();
        System.out.print("Masukkan Jumlah Barang : ");
        int jumlahBarang = input.nextInt();

        int[] array = new int[jumlahBarang];
        int[] arrayHargaBarang = new int[jumlahBarang];
        String[] arrayNamaBarang = new String[jumlahBarang];
        System.out.println();

        for(int i = 0; i < array.length; i++) {
            System.out.print("Masukkan Nama Barang : ");
            input.nextLine();
            String namaBarang = input.nextLine();
            arrayNamaBarang[i] = namaBarang;
            System.out.print("Masukkan Harga Barang : Rp. ");

            int hargaBarang = input.nextInt();
            arrayHargaBarang[i] = hargaBarang;
            System.out.println();
        }

        String output = "";

        for(int i = 0; i < array.length; i++) {
            if (jumlahUang >= arrayHargaBarang[i]) {
                jumlahUang -= arrayHargaBarang[i];
                output = output + arrayNamaBarang[i] + ", ";
            }
        }

        System.out.println(output);
        System.out.println("Uang kembalian yg diterima : Rp. " +jumlahUang);
    }
}


