package LogicPractice;

import java.util.Scanner;
public class Soal2 {
    public static void Resolve() {

        Scanner input = new Scanner(System.in);

        System.out.println("Menghitung Waktu Tempuh  : ");
        System.out.println("1. Grosir X");
        System.out.println("2. Toko 1");
        System.out.println("3. Toko 2");
        System.out.println("4. Toko 3");
        System.out.println("5. Toko 4");

        System.out.print("Masukkan banyaknya rute: ");
        int rute  = input.nextInt();
        int[] array= new int[rute];
        int jarak = 0;
        int waktuToko = 0;

        int i;
        for(i = 0; i < array.length; i++) {
            System.out.print("Masukkan rute ke- "+ (i + 1) + " : ");
            int ruteOption = input.nextInt();
            array[i] = ruteOption;
        }

        for(i = 0; i < array.length - 1; i++) {
            if (array[i] == 1 && array[i + 1] == 2) {
                jarak += 500;
                waktuToko += 10;
            } else if (array[i] == 1 && array[i + 1] == 3) {
                jarak += 2000;
                waktuToko += 10;
            } else if (array[i] == 1 && array[i + 1] == 4) {
                jarak += 3500;
                waktuToko += 10;
            } else if (array[i] == 1 && array[i + 1] == 5) {
                jarak += 5000;
                waktuToko += 10;
            } else if (array[i] == 2 && array[i + 1] == 1) {
                jarak += 500;
                waktuToko += 10;
            } else if (array[i] == 2 && array[i + 1] == 3) {
                jarak += 1500;
                waktuToko += 10;
            } else if (array[i] == 2 && array[i + 1] == 4) {
                jarak += 3000;
                waktuToko += 10;
            } else if (array[i] == 2 && array[i + 1] == 5) {
                jarak += 3500;
                waktuToko += 10;
            } else if (array[i] == 3 && array[i + 1] == 1) {
                jarak += 2000;
                waktuToko += 10;
            } else if (array[i] == 3 && array[i + 1] == 2) {
                jarak += 1500;
                waktuToko += 10;
            } else if (array[i] == 3 && array[i + 1] == 4) {
                jarak += 1500;
                waktuToko += 10;
            } else if (array[i] == 3 && array[i + 1] == 5) {
                jarak += 3000;
                waktuToko += 10;
            } else if (array[i] == 4 && array[i + 1] == 1) {
                jarak += 3500;
                waktuToko += 10;
            } else if (array[i] == 4 && array[i + 1] == 2) {
                jarak += 3000;
                waktuToko += 10;
            } else if (array[i] == 4 && array[i + 1] == 3) {
                jarak += 1500;
                waktuToko += 10;
            } else if (array[i] == 4 && array[i + 1] == 5) {
                jarak += 1500;
                waktuToko += 10;
            } else if (array[i] == 5 && array[i + 1] == 1) {
                jarak += 5000;
                waktuToko += 10;
            } else if (array[i] == 5 && array[i + 1] == 2) {
                jarak += 4500;
                waktuToko += 10;
            } else if (array[i] == 5 && array[i + 1] == 3) {
                jarak += 3000;
                waktuToko += 10;
            } else if (array[i] == 5 && array[i + 1] == 4) {
                jarak += 1500;
                waktuToko += 10;
            }
        }
        int waktuTempuh = jarak/500;
        int waktuDrop = waktuToko-10;

        System.out.println("Waktu Tempuh = " + waktuTempuh + " menit");
        System.out.println("Waktu Drop Barang = " + waktuDrop + " menit");
        System.out.println("Total Waktu Tempuh = " + (waktuTempuh+waktuDrop) + " menit");
    }

    }

