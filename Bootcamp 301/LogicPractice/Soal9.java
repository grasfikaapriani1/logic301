package LogicPractice;
import java.util.Scanner;

public class Soal9 {
    public static void Resolve() {
                Scanner sc = new Scanner(System.in);
                //array yang dibutuhkan
                double []jamMasuk = new double[1];
                double []menitMasuk = new double[1];

                double []jamKeluar = new double[1];
                double []menitKeluar = new double[1];
                double []bayar = new double[1];
                double total = 0;

                //masukkan data
                for(int i=0; i<1; i++)
                {
                    System.out.print("Jam Masuk    : ");
                    jamMasuk[i] = sc.nextDouble();
                    System.out.print("Menit Masuk    : ");
                    menitMasuk[i] = sc.nextDouble();
                    System.out.print("Jam Keluar    : ");
                    jamKeluar[i] = sc.nextDouble();
                    System.out.print("Menit Keluar    : ");
                    menitKeluar[i] = sc.nextDouble();


                    //hitung lama parkir untuk ditampilkan
                    Double jamParkir = (jamKeluar[i]-jamMasuk[i]);
                    Double menitParkir = (menitKeluar[i]-menitMasuk[i]);
                    if(menitParkir<0){
                        jamParkir = jamParkir-1;
                        menitParkir = menitParkir+60;
                    }
                    System.out.println("Lama parkir    : "+jamParkir+"jam " +menitParkir+"menit");

                    //hitung lama parkir untuk hitung biaya
                    double lamaParkir = Math.ceil((((jamKeluar[i]*60)+menitKeluar[i])-((jamMasuk[i]*60)+menitMasuk[i]))/60);

                    //hitung dan tampilkan biaya parkir
                    if(lamaParkir <= 8){
                        bayar[i]=1000;
                    }
                    else if  (lamaParkir > 8 && lamaParkir <= 24){
                        bayar[i]=8000;
                    }
                    else if (lamaParkir > 24){
                        double lamaParkir24 = lamaParkir-24;
                        if (lamaParkir24 <= 8 ){
                            bayar[i]= (lamaParkir24*1000)+15000;
                        }
                        else if (lamaParkir24 > 8 && lamaParkir24 < 24){
                            bayar[i] = 15000+8000;
                        }
                }
                    System.out.println("Biaya Parkir    : "+bayar[i]);
                    System.out.println();

                    //hitung total parkir
                    total = total+bayar[i];
                }

                System.out.println("Total tarif parkir yg harus dibayarkan : "+total);
            }
        }

