package LogicPractice;
import java.util.Scanner;
public class Soal7 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Deskripsi simbol: ");
        System.out.println("- = jalan \to = lubang");
        System.out.println("w = berjalan \tj = lompat");

        //Input pola
        System.out.print("Input pola : ");
        String pola = input.nextLine();
        char[] arrayPola = pola.toCharArray();

        //Input pola cara jalan
        System.out.print("Input cara jalan : ");
        String jalan = input.nextLine();
        char[] arrayjalan = jalan.toCharArray();

        //Definisi Variabel
        int energi = 0;

        int i=0;
        int j=0;
        while (i < arrayPola.length) {
            while(j<arrayjalan.length){
                if (arrayPola[i] == '-') {
                    if (arrayjalan[j] == 'w') {
                        energi += 1;
                        if (energi <= 0) {
                            System.out.println("Jim Died");
                            return;
                        }
                    } else if (arrayjalan[j] == 'j') {
                        energi -= 2;
                        if (energi <= 0) {
                            System.out.println("Jim Died");
                            return;
                        }
                        i = i + 1;
                    }
                } else if (arrayPola[i] == 'o') {
                    if (arrayjalan[j] == 'j') {
                        energi -= 2;
                        if (energi <= 0) {
                            System.out.println("Jim Died");
                            return;
                        }
                        i = i + 1;
                    } else if (arrayjalan[j] == 'w') {
                        System.out.println("Jim Died");
                        return;
                    }
                }
               j++;
            }
           i++;
        }
        System.out.println("Energi Jim : " + energi);
    }
}

