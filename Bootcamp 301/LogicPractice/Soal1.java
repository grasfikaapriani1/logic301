package LogicPractice;

import java.util.Scanner;
public class Soal1 {

        public static void Resolve() {
            //Input panjang deret
            Scanner input = new Scanner(System.in);
            System.out.print("Masukkan panjang deret = ");
            int n = input.nextInt();

            //Definisikan variabel
            int bilanganKelipatan3 = 3; //Kelipatan 3
            int bilanganKelipatan2 = -2; //Kelipatan (-2)*1
            int helper = 0;

            //Definisikan array
            int[] arrayKelipatan3 = new int[n];
            int[] arrayKelipatan3Min1 = new int[n];
            int[] arrayKelipatan2 = new int[n];
            int[] arraySum = new int[n];

            //Deret Kelipatan 3
            int i;
            for (i = 0; i < arrayKelipatan3.length; i++) {
                arrayKelipatan3[i] = bilanganKelipatan3;
                bilanganKelipatan3 += 3;
            }
            //Deret Kelipatan 3-1
            for (i = 0; i < arrayKelipatan3Min1.length; i++) {
                arrayKelipatan3Min1[i] = arrayKelipatan3[i] - 1;
                System.out.print(arrayKelipatan3Min1[i] + " ");
            }
            System.out.println();
            //Deret Kelipatan (-2)*1
            for (i = 0; i < arrayKelipatan2.length; i++) {
                System.out.print(bilanganKelipatan2 + " ");
                arrayKelipatan2[i] = bilanganKelipatan2;
                bilanganKelipatan2 += (-2) * 1;
            }
            System.out.println("\n");
            System.out.println("Output : ");
            for (i = 0; i < arrayKelipatan3Min1.length; i++) {
                arraySum[i] = arrayKelipatan3Min1[i] + arrayKelipatan2[i]; //Menghitung penjumlahan indeks
                System.out.print(arraySum[i] + " ");
            }
        }
    }
