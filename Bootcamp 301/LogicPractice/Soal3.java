package LogicPractice;

import java.util.Arrays;
import java.util.Scanner;
public class Soal3 {
    public static void Resolve() {
        System.out.print("Banyak inputan penjualan : ");
        Scanner input = new Scanner(System.in);
        int banyakInputan= input.nextInt();
        String[] recordpenjualan = new String[banyakInputan];
        String[][] recordpenjualansplit = new String[banyakInputan][2];
        System.out.println();
        int helper = 0;
        int i = 0;

        while(true) {
            System.out.printf("Input record ke-%d = ", helper + 1);
            Scanner input2 = new Scanner(System.in);
            String inputPenjualan = input2.nextLine();
            if (inputPenjualan.contains(":")) {
                recordpenjualan[helper] = inputPenjualan;
                ++helper;
            } else {
                System.out.println("Format yg anda masukkan salah");
            }

            if (helper == banyakInputan) {
                System.out.println();

                String[] listBuah;
                for(i = 0; i < banyakInputan; i++) {
                    listBuah = recordpenjualan[i].split(":");

                    for(int j = 0; j < listBuah.length; j++) {
                        recordpenjualansplit[i][j] = listBuah[j];
                    }
                }

                String[] buah = new String[banyakInputan];

                for(i = 0; i < banyakInputan; i++) {
                    buah[i] = recordpenjualansplit[i][0].toLowerCase();
                }

                listBuah= (String[]) Arrays.stream(buah).distinct().toArray((x$0) -> {
                    return new String[x$0];
                });
                String[][] summaryRecord = new String[listBuah.length][2];

                int j;
                for(i = 0; i < listBuah.length; i++) {
                    int sumPenjualan = 0;

                    for(j = 0; j < recordpenjualansplit.length; j++) {
                        if (recordpenjualansplit[j][0].toLowerCase().equals(listBuah[i])) {
                            sumPenjualan += Integer.parseInt(recordpenjualansplit[j][1]);
                            summaryRecord[i][0] = listBuah[i];
                            summaryRecord[i][1] = Integer.toString(sumPenjualan);
                        }
                    }
                }

                System.out.println("---Summary Record Penjualan---");

                for(i = 0; i < summaryRecord.length; i++) {
                    for(j = 0; j < summaryRecord[0].length; j++) {
                        System.out.print(summaryRecord[i][j] + " ");
                    }

                    System.out.println();
                }

                break;
            }

           i++;
        }
    }
}
