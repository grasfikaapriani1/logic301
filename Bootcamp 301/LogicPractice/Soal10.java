package LogicPractice;
import java.util.Scanner;

public class Soal10 {
        public static void Resolve() {

            Scanner input = new Scanner(System.in);
            System.out.println("Konversi Volume : ");
            System.out.println("1 Botol = 2 Gelas");
            System.out.println("1 Teko  = 25 Cangkir");
            System.out.println("1 Gelas = 2.5 Cangkir");

            System.out.print("Masukkan volume : ");
            float volume = input.nextFloat();
            input.nextLine();

            System.out.println("Ketikan Konversi : ");
            String konversi=input.nextLine();

            String[] konversiArray = konversi.split("-");

            String botol   ="botol";
            String teko    ="teko";
            String cangkir ="cangkir";
            String gelas   ="gelas";
            double hasil;

            System.out.println();
            System.out.println("Output : ");
            for(int i = 0; i < konversiArray.length - 1; i++) {
                if (konversiArray[i].equals(botol) && konversiArray[i+1].equals(teko)) {
                    hasil = (float) volume*0.2;
                    System.out.println( volume+" botol = "+hasil+ " teko");
                } else if (konversiArray[i].equals(botol) && konversiArray[i+1].equals(gelas)) {
                    hasil = (float) volume*2;
                    System.out.println( volume+" botol = "+hasil+ " gelas");
                } else if (konversiArray[i].equals(botol) && konversiArray[i+1].equals(cangkir)) {
                    hasil = (float) volume*5;
                    System.out.println( volume+" botol = "+hasil+ " cangkir");
                } else if (konversiArray[i].equals(teko) && konversiArray[i+1].equals(cangkir)) {
                    hasil = (float) volume*25;
                    System.out.println( volume+" teko = "+hasil+ " cangkir");
                } else if (konversiArray[i].equals(teko) && konversiArray[i+1].equals(gelas)) {
                    hasil = (float) volume*10;
                    System.out.println( volume+" teko = "+hasil+ " gelas");
                } else if (konversiArray[i].equals(teko) && konversiArray[i+1].equals(botol)) {
                    hasil = (float) volume*5;
                    System.out.println( volume+" teko = "+hasil+ " botol");
                } else if (konversiArray[i].equals(gelas) && konversiArray[i+1].equals(cangkir)) {
                    hasil = (float) volume*2.5;
                    System.out.println( volume+" gelas = "+hasil+ " cangkir");
                } else if (konversiArray[i].equals(gelas) && konversiArray[i+1].equals(botol)) {
                    hasil = (float) volume*0.5;
                    System.out.println( volume+" gelas = "+hasil+ " botol");
                } else if (konversiArray[i].equals(gelas) && konversiArray[i+1].equals(teko)) {
                    hasil = (float) volume*0.1;
                    System.out.println( volume+" gelas = "+hasil+ " teko");
                } else if (konversiArray[i].equals(cangkir) && konversiArray[i+1].equals(teko)) {
                    hasil = (float) volume*0.04;
                    System.out.println( volume+" cangkir = "+hasil+ " teko");
                } else if (konversiArray[i].equals(cangkir) && konversiArray[i+1].equals(botol)) {
                    hasil = (float) volume*0.2;
                    System.out.println( volume+" cangkir = "+hasil+ " botol");
                } else if (konversiArray[i].equals(cangkir) && konversiArray[i+1].equals(gelas)) {
                    hasil = (float) volume*0.4;
                    System.out.println( volume+" cangkir = "+hasil+ " gelas");
                }
            }
        }
    }
