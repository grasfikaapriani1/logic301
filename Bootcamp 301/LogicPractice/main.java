package LogicPractice;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while(answer.toUpperCase().equals("Y"))
        {
            System.out.print("Please enter the question number: ");
            int number = input.nextInt();

            switch (number)
            {
                case 1:
                   Soal1.Resolve();
                    break;
                case 2:
                    Soal2.Resolve();
                    break;
                case 3:
                    Soal3.Resolve();
                    break;
                case 4:
                    Soal4.Resolve();
                    break;
                case 5:
                    Soal5.Resolve();
                    break;
                case 6:
                    Soal6.Resolve();
                    break;
                case 7:
                    Soal7.Resolve();
                    break;
                case 8:
                    Soal8.Resolve();
                    break;
                case 9:
                    Soal9.Resolve();
                    break;
                case 10:
                    Soal10.Resolve();
                    break;
                case 11:
                    Soal11.Resolve();
                    break;
                case 12:
                    Soal12.Resolve();
                    break;
                case 13:
                    Soal13.Resolve();
                    break;
                default:
                    System.out.println("No question available");
                    break;
            }

            System.out.println();
            System.out.print("Again? (Y/N) : ");
            input.nextLine();
            answer = input.nextLine();
        }

        System.out.println("Thank you. See you soon");

    }
}
